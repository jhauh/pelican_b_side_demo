Pelican B-Side Demo
###################

Source and HTML for an example site to show off 
`Pelican B-side theme <https://gitlab.com/jhauh/pelican_b_side>`_.

View it here: https://jhauh.gitlab.io/pelican_b_side_demo

All of the material for this demo site was taken from the original
`Hugo B-side demo <https://github.com/fisodd/hugo-b-side>`_, and has
been left in its original state as much as possible. Therefore, there
are plenty of references to Hugo, which is a bit silly. Nonetheless,
the content shows off many of the features of the theme.
